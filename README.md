# Token

简介：主要使用jwt示例。

- **signature** : 用于签名使用，生产公钥私钥。
- **gjwt**: 包含多个不同类型的jwt认证
- **session**: 服务端返回校验session 

参考: [ github.com/marmotedu/iam]( github.com/marmotedu/iam)

[[component-base@v1.6.2](..%2F..%2F..%2Fpkg%2Fmod%2Fgithub.com%2Fmarmotedu%2Fcomponent-base%40v1.6.2)]


# 第三方

- [github.com/minio/pkg/net](github.com/minio/pkg/net)
- [github.com/minio/pkg/certs](github.com/minio/pkg/certs)
- [github.com/buger/jsonparser](github.com/buger/jsonparser)

# 额外扩展

- [https://learnku.com/docs/cargo-book/2018/specifying-dependencies/4773#dddf69](https://learnku.com/docs/cargo-book/2018/specifying-dependencies/4773#dddf69)