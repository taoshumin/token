/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package session

import (
	"context"
	"reflect"
	"testing"
	"time"
)

func TestJWT_Create(t *testing.T) {
	type fields struct {
		Secret  string
		Jwksurl string
		Now     func() time.Time
	}
	type args struct {
		ctx  context.Context
		user Principal
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    Token
		wantErr bool
	}{
		{
			name: "create jwt",
			fields: fields{
				Secret:  "secret",
				Jwksurl: "www.xiadata.io",
				Now:     DefaultNowTime,
			},
			args: args{
				ctx: context.Background(),
				user: Principal{
					KeyID:        "shumintao",
					Subject:      "karma",
					Issuer:       "www.xiadata.com",
					Organization: "www.xiadata.com",
					Group:        "www.xiadata.com",
					ExpiresAt:    time.Now().Add(10 * time.Minute),
					IssuedAt:     time.Now(),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := &JWT{
				Secret:  tt.fields.Secret,
				Jwksurl: tt.fields.Jwksurl,
				Now:     tt.fields.Now,
			}
			got, err := j.Create(tt.args.ctx, tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != 0 {
				t.Errorf("Create() got = %v", got)
			}
		})
	}
}

func TestJWT_ValidPrincipal(t *testing.T) {
	type args struct {
		ctx      context.Context
		jwtToken Token
		lifespan time.Duration
	}
	tests := []struct {
		name    string
		args    args
		want    Principal
		wantErr bool
	}{
		{
			name: "valid token",
			args: args{
				ctx:      context.Background(),
				jwtToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDc4NDUyNTUsImlhdCI6MTY0Nzg0NDY1NSwiaXNzIjoid3d3LnhpYWRhdGEuY29tIiwibmJmIjoxNjQ3ODQ0NjU1LCJzdWIiOiJrYXJtYSIsIm9yZyI6Ind3dy54aWFkYXRhLmNvbSIsImdycCI6Ind3dy54aWFkYXRhLmNvbSJ9.xb2OK1_PJV9fWCVLCUsyvxsu7gRXSWoUQBqGLaoldWQ",
				lifespan: time.Second,
			},
			want:    Principal{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := NewJWT("secret", "www.xiadata.io")
			got, err := j.ValidPrincipal(tt.args.ctx, tt.args.jwtToken, tt.args.lifespan)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidPrincipal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ValidPrincipal() got = %v, want %v", got, tt.want)
			}
		})
	}
}
