/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package session

import (
	"context"
	"time"

	gojwt "github.com/golang-jwt/jwt/v4"
)

const (
	kind  = "karma.io"
	Group = "karma.io"
)

// Principal is any entity that can be authenticated
// copy from https://github.com/influxdata/chronograf/tree/master/oauth2
type Principal struct {
	KeyID        string    `json:"key_id"`
	Subject      string    `json:"subject"`
	Issuer       string    `json:"issuer"`
	Organization string    `json:"organization"`
	Group        string    `json:"group"`
	ExpiresAt    time.Time `json:"expires_at"`
	IssuedAt     time.Time `json:"issued_at"`
}

func NewDefaultPrincipal(kid string, expire time.Time) Principal {
	return Principal{
		KeyID:        kid,
		Subject:      kind,
		Issuer:       kind,
		Organization: Group,
		Group:        Group,
		ExpiresAt:    expire,
		IssuedAt:     time.Now(),
	}
}

// Token represents a time-dependent reference (i.e. identifier) that maps back
// to the sensitive data through a tokenization system
type Token string

// Tokenizer substitutes a sensitive data element (Principal) with a
// non-sensitive equivalent, referred to as a token, that has no extrinsic
// or exploitable meaning or value.
type Tokenizer interface {
	// Create issues a token at Principal's IssuedAt that lasts until Principal's ExpireAt
	Create(context.Context, Principal) (Token, error)
	// ValidPrincipal checks if the token has a valid Principal and requires
	// a lifespan duration to ensure it complies with possible server runtime arguments.
	ValidPrincipal(ctx context.Context, token Token, lifespan time.Duration) (Principal, error)
	// ExtendedPrincipal adds the extention to the principal's lifespan.
	ExtendedPrincipal(ctx context.Context, principal Principal, extension time.Duration) (Principal, error)
	// GetClaims returns a map with verified claims
	GetClaims(tokenString string) (gojwt.MapClaims, error)
}
