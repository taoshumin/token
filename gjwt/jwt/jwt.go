/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package jwt

import (
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"os"
	"time"
)

var SECRETKEY string = os.Getenv("SECRET_KEY")

// JWT copy from https://github.com/krishpranav/golang-management/blob/master/controllers/userController.go
type JWT struct {
	UserID   string
	Username string
	jwt.StandardClaims
}

func Create(uid, username string) (string, string, error) {
	claims := &JWT{
		UserID:   uid,
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Hour * time.Duration(24)).Unix(),
		},
	}

	refreshClaims := &JWT{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Hour * time.Duration(168)).Unix(),
		},
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(SECRETKEY))
	refreshToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshClaims).SignedString([]byte(SECRETKEY))
	if err != nil {
		return "", "", err
	}

	return token, refreshToken, nil
}

func ValidateToken(signedToken string) (*JWT, error) {
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JWT{},
		func(t *jwt.Token) (interface{}, error) {
			return []byte(SECRETKEY), nil
		},
	)
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*JWT)
	if !ok {
		return nil, errors.New("token is expired")
	}

	if claims.ExpiresAt < time.Now().Local().Unix() {
		return nil, errors.New("token is expired")
	}
	return claims, nil
}
