module gitlab.com/taoshumin/token

go 1.18

require (
	github.com/buger/jsonparser v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/json-iterator/go v1.1.12
	github.com/lestrrat-go/jwx/v2 v2.0.8
	github.com/stretchr/testify v1.8.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.1.0 // indirect
	github.com/goccy/go-json v0.9.11 // indirect
	github.com/lestrrat-go/blackmagic v1.0.1 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.4 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/option v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220427172511-eb4f295cb31f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
